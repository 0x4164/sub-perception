from Subject import Subject
from Crawler import Crawler
from Analyzer import Analyzer

class Main:
    def __init__(self):
        a=1

    def run(self):
        subjects = [
            Subject("Universitas Indonesia",[
                "UI","ui"]
            ),
            Subject("Universitas Gadjah Mada",[
                "UGM","ugm"]
            ),
        ]
        
        for su in subjects:
            print("="*32)
            c = Crawler(su,[
                "url1",
                "url2",
            ])
            c.crawl()

            a = Analyzer(su)
            a.analyze()

        return 1

m = Main()
m.run()