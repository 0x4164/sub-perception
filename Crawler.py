# import requests
# import beautifulsoups
import random

class Crawler:
    # search subject in urls
    def __init__(self, subject, urls):
        self.subject = subject
        self.urls = urls
        a=1
    
    # crawl from single url
    def crawlSingle(self, keywords, url):
        # res = keyword+" "+str(res)+" kwds found"
        # to do : implement crawl from url
        print(self.subject.fullname,"::",url)
        
        content = ["ub"]*random.randint(1,10)
        content = " ".join(content)
        print(keywords)
        print(content)
        
        res = any(x in content for x in keywords)
        print(res)
        
        return res

    # crawl from all urls
    def crawl(self):
        print("crawling..")
        crawlResult=[]
        for i,url in enumerate(self.urls):
            # res="crawl result %d"%i
            keywords = self.subject.getKeywords()
            res = self.crawlSingle(keywords, url)
            crawlResult.append(res)

        self.subject.setCrawlResult(crawlResult)

        print("finished crawling..")

        return 1
    
    def toFile(self,name):
        f = open(name,'w').write()
        return 1