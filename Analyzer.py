import random

class Analyzer:
    # analyze subject
    def __init__(self, subject):
        self.subject = subject
    
    def setPersepsi(self, subject):
        ppos=random.randint(1,100)
        pneg=100-ppos
        
        persepsi = {
            "positif":ppos,
            "negatif":pneg
        }
        
        return persepsi


    def analyze(self):
        print("analyzing..")
        pers = self.setPersepsi(self.subject)
        self.subject.setPersepsi(pers)

        print(vars(self.subject))

        return 1